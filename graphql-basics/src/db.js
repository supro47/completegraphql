const users = [
    {
        id: '1',
        name: 'Aury',
        email: 'aury@aury.com',
        age: 31
    },
    {
        id: '2',
        name: 'Eri',
        email: 'eri@aury.com',
    },
    {
        id: '3',
        name: 'Nory',
        email: 'nory@aury.com',
    },
]

const posts = [
    {
        id: '11',
        title: 'The First Post',
        body: 'How much wood could a wood',
        published: true,
        author: '1'
    },{
        id: '12',
        title: 'Second',
        body: 'chuck chuck if a wood chuck',
        published: true,
        author: '2'
    },{
        id: '13',
        title: "3rd",
        body: 'could chuck wood',
        published: false,
        author: '1'
    },
]

const comments = [
    {
        id: '111',
        text: 'This is my favorite post',
        author: '2',
        post: '13'
    }, {
        id: '112',
        text: 'I didn\'t understand this',
        author: '2',
        post: '12'
    }, {
        id: '113',
        text: 'WOOD CHUCKS!',
        author: '1',
        post:'13'
    }, {
        id: '114',
        text: 'Wow. So inspirational!',
        author: '3',
        post: '11'
    }
]

const db = {
    users,
    posts,
    comments
}

export { db as default }