const Query = {
    me() {
        return {
            id: 'abc123',
            name: 'Aury',
            email: 'aury@aury.com'
        }
    },

    post() {
        return {
            id: '1234',
            title: 'First post',
            body: 'This is a post',
            published: true
        }
    },

    users(parent, args, { db }, info) {
        if (!args.query) {
            return db.users
        } else {
            return db.users.filter((user) => {
                return user.name.toLowerCase().includes(args.query.toLowerCase())
            })
        }
    }, 
    posts(parent, args, { db }, info) {
        if(!args.query) {
            return db.posts
        } else {
            return db.posts.filter((post) => {
                return post.title.toLowerCase().includes(args.query.toLowerCase()) || post.body.toLowerCase().includes(args.query.toLowerCase())
            })
        }
    },
    comments(parent, args, { db }, info) {
        if(!args.query) {
            return db.comments
        } else {
            return db.comments.filter((comment) => {
                return comment.text.toLowerCase().includes(args.query.toLowerCase())
            })
        }
    }

}

export { Query as default }